#!/bin/bash
# Script for copying ParaK to pawnee machine

echo "Copying ParaK to pawnee machine"
mkdir Empty
scp -r Empty pawnee:ParaK
scp -r Empty pawnee:ParaK/data
scp -r src pawnee:ParaK/src
scp run_Intel.sh pawnee:ParaK/run.sh
scp src/Makefile_Intel pawnee:ParaK/src/Makefile
scp Parameters.txt pawnee:ParaK
rmdir Empty

ssh pawnee
