#!/bin/bash
# Script for copying ParaK to tibidabo machine

echo "Copying ParaK to tibidabo machine"
mkdir Empty
scp -r Empty alegrand@tibidabo.bsc.es:ParaK
scp -r Empty alegrand@tibidabo.bsc.es:ParaK/data
scp -r src alegrand@tibidabo.bsc.es:ParaK/src
scp run.sh alegrand@tibidabo.bsc.es:ParaK
scp Parameters.txt alegrand@tibidabo.bsc.es:ParaK
scp all_run.sh alegrand@tibidabo.bsc.es:ParaK
scp setup* alegrand@tibidabo.bsc.es:ParaK
rm -r Empty

ssh tibidabo

