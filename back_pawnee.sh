#!/bin/bash
# Script for copying data from pawnee machine to my machine 

echo "Copying data from pawnee machine to my machine"
#Producing the original name for data folder
bkup=1
while [ -e dataP${bkup} ] ; do 
  bkup=`expr $bkup + 1`
done
scp -r pawnee:ParaK/data dataP${bkup}
