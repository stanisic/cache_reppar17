// This is code for ParaK for ARM

// Code for debugging:
// gdb src/program_ARM core data/ParaKKernel3.dat data/ParaKData3.dat 1 0 1 42 4 2100 1024 0 0

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>
#include <pthread.h>
#include <sched.h>
#include <sys/types.h>

#include <arm_neon.h>

#define BILLION  1000000000L //In order to divide nanoseconds and get result in seconds

typedef enum { FALSE, TRUE } boolean;

// Variables of the experiment
int64x2_t* vmainBuffer;
TYPE* mainBuffer;
TYPE result;

// Main methods for the experiments
void measure(FILE *fpdat, int m_num, int m_stride, int m_mem, int m_cycles, int m_mode, int m_exemode);

//Method for memory allocation
TYPE* allocation(int buffer_size);
int64x2_t* allocation128(int buffer_size);

// Thread parameters and method
unsigned long int n_thr, thr_counter;
int* thr_core;
struct thread_data{
  int td_id;
  TYPE td_sum;
  TYPE* td_buffer;
  int64x2_t* td_vectbuffer;
  struct timespec td_time_start, td_time_stop;
  int td_start, td_cycles, td_stride, buffer_size, td_exemode; 
};
struct thread_data* thread_data_array;
pthread_mutex_t count_mutex;
pthread_cond_t finished_cond, count_cond;
boolean kill_threads;
void *thread_func(void *threadid);
void pincore(unsigned long int core);

// Input file parameters
int inp_seed, inp_allocmode, inp_static_size, inp_type_mem, inp_accesses_sqr, main_core, inp_ubuffer;
char inputfile[50], outputfile[50];
void kernelparameters(char* argv[]);

// Time variables and methods
struct timespec tot_start, tot_stop, tot_dif;
struct timespec start, stop, dif;
long time_offset;
void diff(struct timespec* start, struct timespec* end, struct timespec* result_time);
void time_start(void);
void time_end(void);

//Execution modes
int exemode1(int m_start,int m_cycles,int m_stride,int buffer_size, TYPE* buffer);
int exemode2(int m_start,int m_cycles,int m_stride,int buffer_size, TYPE* buffer);
int exemode3(int m_start,int m_cycles,int m_stride,int buffer_size, int64x2_t* vectBuffer, int tid);
int exemode4(int m_start,int m_cycles,int m_stride,int buffer_size, int64x2_t* vectBuffer, int tid);

int main(int argc, char* argv[])
//argv[*]= ParaKInput.dat ParaKData#.dat n_thr thr_core inp_seed inp_allocmode inp_static_size, inp_type_mem, inp_accesses_sqr
{
  // Measurement variables
  int m_num, m_stride, m_mem, m_cycles, m_mode, m_exemode;

  // Initialization
  time_start();    
  kernelparameters(argv);
  pincore(main_core);
  result=0;
  srand(inp_seed);

  //Opening files and writing header
  char line[200];
  FILE *fpInput, *fpdat;
  fpInput=fopen(inputfile, "r");
  if (fpInput==NULL) printf("Error while opening the .dat file");  
  fpdat=fopen(outputfile, "a+");
  if (fpdat==NULL) printf("Error while opening the .dat file");  
  fprintf(fpdat, "\n################################################\n# MEASUREMENTS=\n");
  fprintf(fpdat, " NUM\t THREAD\t CORE\t STRIDE\t SIZE\t ADDRESS    START_TIME\t\t END_TIME\t\t TIME\t\t BANDWIDTH\t MODE\t EX\n"); 

  //Malloc only once one big memory size if the threads are sharing the buffer
  int buffer_size = inp_static_size*inp_type_mem/sizeof(TYPE);
  if (VECTOR == 1)
    buffer_size = inp_static_size*inp_type_mem/sizeof(int64x2_t);
  if(inp_ubuffer == 0 && (inp_allocmode==2 || inp_allocmode==4) && VECTOR==0)
    mainBuffer = allocation(buffer_size);
  if(inp_ubuffer == 0 && (inp_allocmode==2 || inp_allocmode==4) && VECTOR==1)
    vmainBuffer = allocation128(buffer_size);
  
  // Child threads (workers)
  pthread_t threads[n_thr];
  struct thread_data temp[n_thr];
  thread_data_array=temp;
  int rc, t;
  // Initialize mutex and condition variable objects 
  thr_counter=0;
  kill_threads = FALSE;
  pthread_mutex_init(&count_mutex, NULL);
  pthread_cond_init(&count_cond, NULL);
  pthread_cond_init(&finished_cond, NULL);
  pthread_mutex_lock(&count_mutex);
  // Creating threads 
  for(t=0; t<n_thr; t++){
    //printf("In main: creating thread %ld\n", t);
    thread_data_array[t].td_id = t;
    thread_data_array[t].td_sum = 0;
    //Linking the buffer(s) to threads
    if (VECTOR == 0 && (inp_allocmode==2 || inp_allocmode==4))
       {
	 if(inp_ubuffer == 0)
	   thread_data_array[t].td_buffer = mainBuffer;
	 else
	   thread_data_array[t].td_buffer = allocation(buffer_size);
       }
     else if (VECTOR == 1 && (inp_allocmode==2 || inp_allocmode==4))
       {
	 if(inp_ubuffer == 0)
	   thread_data_array[t].td_vectbuffer = vmainBuffer;
	 else
	   thread_data_array[t].td_vectbuffer = allocation128(buffer_size);
       }
    rc = pthread_create(&threads[t], NULL, thread_func, (void *)t);
    if (rc){
      printf("ERROR; return code from pthread_create() is %d\n", rc);
      exit(-1);
    }
  }
  //printf("Blocked until threads are ready!\n");
  pthread_cond_wait(&finished_cond, &count_mutex);
  pthread_mutex_unlock(&count_mutex);
  // Executing experiments
  while (fgets(line, 200, fpInput) != NULL)
    {
      sscanf (line, "%d %d %d %d %d %d",&m_num, &m_stride, &m_mem, &m_cycles, &m_mode, &m_exemode);
      measure(fpdat, m_num, m_stride, m_mem, m_cycles, m_mode, m_exemode);
    } 
  
  // Killing the child threads (workers)
  kill_threads = TRUE;
  pthread_cond_broadcast(&count_cond);
  pthread_mutex_unlock(&count_mutex);
  time_end();

  // Printing the output
  printf("ID of this measurement = %d; Total time: %lf\n", result,tot_dif.tv_sec+(double)(tot_dif.tv_nsec/(double)BILLION));
  fprintf(fpdat,"# ID of this measurement = %d; Total time: %lf sec\n", result,tot_dif.tv_sec+(double)(tot_dif.tv_nsec/(double)BILLION));  
  
  // Deallocating buffers, closing files
  if(inp_ubuffer == 0 && (inp_allocmode==2 || inp_allocmode==4) && VECTOR==0)
      free(mainBuffer);
  if(inp_ubuffer == 0 && (inp_allocmode==2 || inp_allocmode==4) && VECTOR==1)
      free(vmainBuffer);
  free(thr_core);
  fclose(fpInput);
  fclose(fpdat);
  
  // Waiting for child threads (workers) to die and then ending
  pthread_exit(NULL);
}

TYPE* allocation(int buffer_size)
{
  TYPE* buffer;
  int i;
  //  buffer = (TYPE*) malloc (buffer_size*sizeof(TYPE));
posix_memalign((void**)&buffer,256,buffer_size*sizeof(TYPE));
  if (buffer==NULL) printf("Error while allocation memory for buffer");
  for(i=0; i<buffer_size; i++)
    {
      buffer[i]=(TYPE)rand();
    }
  return buffer;
}

int64x2_t* allocation128(int buffer_size)
{
  int i;
  int64x2_t* vectBuffer;
  //  vectBuffer = (int64x2_t*) malloc (buffer_size*sizeof(int64x2_t));
posix_memalign((void**)&vectBuffer,256,buffer_size*sizeof(int64x2_t));
  if (vectBuffer==NULL) printf("Error while allocation memory for buffer");
  for(i=0; i<buffer_size; i++)
    {
      vectBuffer[i]=vmovq_n_s64(rand());
    }
  return vectBuffer;
}

// Reading kernel parameters from input file
void kernelparameters(char* argv[])
//argv[*]= ParaKInput.dat ParaKData#.dat n_thr thr_core inp_seed inp_allocmode inp_static_size, inp_type_mem, inp_accesses_sqr
{
  int argm=1;
  strcpy(inputfile, argv[argm++]);
  strcpy(outputfile, argv[argm++]);
  n_thr=atoi(argv[argm++]);
  
  thr_core = (int*) malloc(n_thr*sizeof(int));
  main_core = atoi(argv[argm++]);
  int i;
  for(i=0;i<n_thr;i++)
   thr_core[i]=atoi(argv[argm++]);
 
  inp_seed=atoi(argv[argm++]);
  inp_allocmode=atoi(argv[argm++]);
  inp_static_size=atoi(argv[argm++]);
  inp_type_mem=atoi(argv[argm++]);
  inp_accesses_sqr=atoi(argv[argm++]);
  inp_ubuffer=atoi(argv[argm++]);
}

// Pinning the processor core to a specific thread
void pincore(unsigned long int core)
{
  if (core!=99)
    {
      // Pin the CPU core
      cpu_set_t  cpu_set;
      CPU_ZERO(&cpu_set);
      CPU_SET(core,&cpu_set);

      /* first bit to mask 1, core 0 */
      sched_setaffinity(0, sizeof(cpu_set_t), &cpu_set) ;
      /* 0 to indicate current process/thread */ 
      sched_yield(); 
      // Finished pin
    }
}

// Start time for the whole program
void time_start(void)
{
  // Computing time offset
  clock_gettime( CLOCK_REALTIME, &tot_start);
  // clock_gettime( CLOCK_REALTIME, &tot_dif); 
  clock_gettime( CLOCK_REALTIME, &tot_stop);
  diff(&tot_start,&tot_stop,&tot_dif);
  time_offset=(long)tot_dif.tv_nsec;
  // Start of program
  clock_gettime( CLOCK_REALTIME, &tot_start); 
}

// Stop time for the whole program
void time_end(void)
{
  clock_gettime( CLOCK_REALTIME, &tot_stop);
  diff(&tot_start,&tot_stop,&tot_dif);
}

// Calculating time difference
void diff(struct timespec* start, struct timespec* end, struct timespec* result_time)
{
	if ((end->tv_nsec-start->tv_nsec)<0)
	{
		result_time->tv_sec = end->tv_sec-start->tv_sec-1;
		result_time->tv_nsec = (double)BILLION+end->tv_nsec-start->tv_nsec;
	} else {
		result_time->tv_sec = end->tv_sec-start->tv_sec;
		result_time->tv_nsec = end->tv_nsec-start->tv_nsec;
	}
}

// Central function of "master thread", that one that is doing measurements
void measure(FILE *fpdat, int m_num, int m_stride, int m_mem, int m_cycles, int m_mode, int m_exemode) 
{
  int i,j,t;
  int sum=0;
  int buffer_size = m_mem/sizeof(TYPE);
  if (VECTOR == 1)
    buffer_size = m_mem/sizeof(int64x2_t);

  // Malloc for every memory size independantly
  if(inp_ubuffer == 0 && (inp_allocmode==1 || inp_allocmode==3) && VECTOR==0)
    mainBuffer = allocation(buffer_size);
  if(inp_ubuffer == 0 && (inp_allocmode==1 || inp_allocmode==3) && VECTOR==1)
    vmainBuffer = allocation128(buffer_size);

  // Choosing starting point when we having a big memory with random access inside it
  int m_start=0;
  if (inp_allocmode==4)
    {
      m_start=rand()%( (inp_static_size*inp_type_mem-m_mem)/sizeof(TYPE));
      if (VECTOR==1)
	m_start=rand()%( (inp_static_size*inp_type_mem-m_mem)/sizeof(int64x2_t));
      // Aligning m_start
      m_start=m_start-(m_start%256);
    }
  
  // Fixing m_exemode for vectorized instructions
  if(m_exemode==1 && VECTOR==1) 
    m_exemode=7;
  if(m_exemode==2 && VECTOR==1) 
    m_exemode=8;
  
  // Setting up threads
   for(t=0; t<n_thr; t++){
     thread_data_array[t].td_start = m_start;
     thread_data_array[t].td_cycles = m_cycles;
     thread_data_array[t].td_stride = m_stride;
     thread_data_array[t].buffer_size = buffer_size;
     if (VECTOR == 0 && (inp_allocmode==1 || inp_allocmode==3))
       {
	 if(inp_ubuffer == 0)
	   thread_data_array[t].td_buffer = mainBuffer;
	 else
	   thread_data_array[t].td_buffer = allocation(buffer_size);
       }
     else if (VECTOR == 1 && (inp_allocmode==1 || inp_allocmode==3))
       {
	 if(inp_ubuffer == 0)
	   thread_data_array[t].td_vectbuffer = vmainBuffer;
	 else
	   thread_data_array[t].td_vectbuffer = allocation128(buffer_size);
       }
     thread_data_array[t].td_exemode = m_exemode;
   }
   pthread_cond_broadcast(&count_cond);
   pthread_mutex_unlock(&count_mutex);
   // Threads are working, waiting for them to finish
   pthread_cond_wait(&finished_cond, &count_mutex);

   // Printing output for each thread
   for(t=0; t<n_thr; t++)
     {
       result+=thread_data_array[t].td_sum;
       start=thread_data_array[t].td_time_start;
       stop=thread_data_array[t].td_time_stop;
       // End time and computing difference
       diff(&start,&stop,&dif);
       dif.tv_nsec-=time_offset;
  
       // Computing BANDWIDTH
       double BANDWIDTH=0;
	   if (m_cycles>1024)	     BANDWIDTH=(double)((m_mem/1024)*(m_cycles/1024))/(double)(dif.tv_sec+(double)dif.tv_nsec/(double)BILLION);
	   else if(m_mem>1024*1024)	     BANDWIDTH=(double)(((m_mem/1024)/1024)*m_cycles)/(double)(dif.tv_sec+(double)dif.tv_nsec/(double)BILLION);
	   else	     BANDWIDTH=(double)(((m_mem*m_cycles)/1024)/(double)(dif.tv_sec+(double)dif.tv_nsec/(double)BILLION))/1024;

       // Writing results into file 
       fprintf(fpdat, "  %d\t %d\t %d\t %d\t %d\t %p  %lf\t %lf\t %lf\t %lf\t %d\t %d\n", m_num, t, thr_core[t],m_stride, m_mem, &mainBuffer[m_start], start.tv_sec+(double)(start.tv_nsec/(double)BILLION), stop.tv_sec+(double)(stop.tv_nsec/(double)BILLION), dif.tv_sec+(double)(dif.tv_nsec/(double)BILLION), BANDWIDTH, m_mode, m_exemode);
  
     }
   // Deallocating buffer in case memory is allocated for each measurement
  if(inp_allocmode==1 && VECTOR==0) free(mainBuffer);
  if(inp_allocmode==1 && VECTOR==1) free(vmainBuffer);
}

// Thread function, code that workers execute 
void *thread_func(void *threadid)
{
  // Getting thread ID (equal to thread_data_array[#].td_id)
  int tid = (int)threadid;
  //printf("Hello World! It's me, thread #%ld on core: %d\n", tid,thr_core[tid]);   
  // Pinning the processor core to this thread
  pincore(thr_core[tid]);
   
  // Thread is ready, now waiting on barrier
  pthread_mutex_lock(&count_mutex);
  thr_counter++;
  if(thr_counter==n_thr){
    thr_counter=0; 
    pthread_cond_signal(&finished_cond);
    pthread_cond_wait(&count_cond, &count_mutex);
  }    
  else
    pthread_cond_wait(&count_cond, &count_mutex);
  pthread_mutex_unlock(&count_mutex);
   
  while(!kill_threads)
    {
      // Executing the small kernel 
      clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_start);
      // Choosing execution mode and executing for loop
      switch( thread_data_array[tid].td_exemode )
	{
	case 1:thread_data_array[tid].td_sum+=exemode1(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size, thread_data_array[tid].td_buffer); clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop); break;
	case 2:thread_data_array[tid].td_sum+=exemode2(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size, thread_data_array[tid].td_buffer); clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop); break;
	case 3:thread_data_array[tid].td_sum+=exemode3(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size,thread_data_array[tid].td_vectbuffer, tid); break;
	case 4:thread_data_array[tid].td_sum+=exemode4(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size,thread_data_array[tid].td_vectbuffer, tid); break;
	case 0:break;
	default :thread_data_array[tid].td_sum+=exemode1(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size, thread_data_array[tid].td_buffer); clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop); break;
	}
   
      // Finished task, now waiting on barrier
      pthread_mutex_lock(&count_mutex);
      thr_counter++;
      if(thr_counter==n_thr){
	thr_counter=0; 
	pthread_cond_signal(&finished_cond);
	pthread_cond_wait(&count_cond, &count_mutex);
      }    
      else
	pthread_cond_wait(&count_cond, &count_mutex);   
      pthread_mutex_unlock(&count_mutex);
    }
   
  // Thread has finished its job
  //printf("Thread #%ld finished!\n", tid);
  pthread_exit(NULL);
}

// ##############################################
// Different small kernels

 //Standard execution  
int exemode1(int m_start, int m_cycles, int m_stride, int buffer_size, TYPE* buffer)
{
  register int i,j,sum=0;

  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride)
	{
	  sum+=buffer[j];
	}
    }
  return sum;
}

//Correct loop unrolling
int exemode2(int m_start, int m_cycles, int m_stride, int buffer_size, TYPE* buffer)
{
  register int i,j, sum=0;
  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride*8)
	{
	  sum+=buffer[j];
	  sum+=buffer[j+m_stride];
	  sum+=buffer[j+2*m_stride];
	  sum+=buffer[j+3*m_stride];
	  sum+=buffer[j+4*m_stride];
	  sum+=buffer[j+5*m_stride];
	  sum+=buffer[j+6*m_stride];
	  sum+=buffer[j+7*m_stride];
	}
    }
  return sum;
}

// Vectorized instructions

//ARM Vectorized instructions 128b without loop unrolling
int exemode3(int m_start,int m_cycles,int m_stride,int buffer_size, int64x2_t* vectBuffer, int tid)
{
//Variables
  int i,j;
  int64_t resultV[2];
  int64x2_t sumV=vmovq_n_s64(0);
 
  //Start time
  clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_start);

  //Kernel
  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride)
	{
	  sumV=vaddq_s64(sumV,vectBuffer[j]);
	}
    }

  //End time and computing difference
  clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop);

  //Computing the result 
   vst1q_s64 (resultV, sumV);

  return (int) (resultV[0]+resultV[1]);
}

 //ARM Vectorized instructions 128b with loop unrolling
int exemode4(int m_start,int m_cycles,int m_stride,int buffer_size, int64x2_t* vectBuffer, int tid)
{
//Variables
  int i,j;
  int64_t resultV[2];
  int64x2_t sumV=vmovq_n_s64(0);
 
  //Start time
  clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_start);

  //Kernel
  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=8*m_stride)
	{
	  sumV=vaddq_s64(sumV,vectBuffer[j]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+m_stride]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+2*m_stride]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+3*m_stride]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+4*m_stride]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+5*m_stride]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+6*m_stride]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+7*m_stride]); 
	}
    }

  //End time and computing difference
  clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop);

  //Computing the result 
   vst1q_s64 (resultV, sumV);

  return (int) (resultV[0]+resultV[1]);
}
