// This is code for ParaK

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>
#include <pthread.h>
#include <sched.h>
#include <sys/types.h>

//For ARM comment this header
#include <immintrin.h>

//For ARM uncomment the header and add to Makefile -mfpu=neon
//#include <arm_neon.h>

#define BILLION  1000000000L //In order to divide nanoseconds and get result in seconds

typedef enum { FALSE, TRUE } boolean;

// Variables of the experiment
__m128i* vmainBuffer;
TYPE* mainBuffer;
TYPE result;
//Only for m_exemode=5
TYPE* pointer;

// Main methods for the experiments
void measure(FILE *fpdat, int m_num, int m_stride, int m_mem, int m_cycles, int m_mode, int m_exemode);

//Method for memory allocation
TYPE* allocation(int buffer_size);
__m128i* allocation128(int buffer_size);

// Thread parameters and method
unsigned long int n_thr, thr_counter;
int* thr_core;
struct thread_data{
  int td_id;
  TYPE td_sum;
  TYPE* td_buffer;
  __m128i* td_vectbuffer;
  struct timespec td_time_start, td_time_stop;
  int td_start, td_cycles, td_stride, buffer_size, td_exemode, td_mem; 
};
struct thread_data* thread_data_array;
pthread_mutex_t count_mutex;
pthread_cond_t finished_cond, count_cond;
boolean kill_threads;
void *thread_func(void *threadid);
void pincore(unsigned long int core);

// Input file parameters
int inp_seed, inp_allocmode, inp_static_size, inp_type_mem, inp_accesses_sqr, main_core, inp_ubuffer;
char inputfile[50], outputfile[50];
void kernelparameters(char* argv[]);

// Time variables and methods
struct timespec tot_start, tot_stop, tot_dif;
struct timespec start, stop, dif;
long time_offset;
void diff(struct timespec* start, struct timespec* end, struct timespec* result_time);
void time_start(void);
void time_end(void);

//Execution modes
int exemode1(int m_start,int m_cycles,int m_stride,int buffer_size, TYPE* buffer);
int exemode2(int m_start,int m_cycles,int m_stride,int buffer_size, TYPE* buffer);
//Vectorized instructions
int exemode7(int m_start,int m_cycles,int m_stride,int buffer_size,__m128i* vectBuffer, int tid);
int exemode8(int m_start,int m_cycles,int m_stride,int m_mem, __m128i* vectBuffer, int tid);
//For ARM comment the block below
/*
int exemode9(int m_start,int m_cycles,int m_stride,int buffer_size,int m_mem);
int exemode10(int m_start,int m_cycles,int m_stride,int buffer_size,int m_mem);
int exemode11(int m_start,int m_cycles,int m_stride,int buffer_size,int m_mem);
*/
int main(int argc, char* argv[])
//argv[*]= ParaKInput.dat ParaKData#.dat n_thr thr_core inp_seed inp_allocmode inp_static_size, inp_type_mem, inp_accesses_sqr
{
  // Measurement variables
  int m_num, m_stride, m_mem, m_cycles, m_mode, m_exemode;

  // Initialization
  time_start();    
  kernelparameters(argv);
  pincore(main_core);
  result=0;
  srand(inp_seed);

  //Opening files and writing header
  char line[200];
  FILE *fpInput, *fpdat;
  fpInput=fopen(inputfile, "r");
  if (fpInput==NULL) printf("Error while opening the .dat file");  
  fpdat=fopen(outputfile, "a+");
  if (fpdat==NULL) printf("Error while opening the .dat file");  
  fprintf(fpdat, "\n################################################\n# MEASUREMENTS=\n");
  fprintf(fpdat, " NUM\t THREAD\t CORE\t STRIDE\t SIZE\t ADDRESS    START_TIME\t\t END_TIME\t\t TIME\t\t BANDWIDTH\t MODE\t EX\n"); 

  //Malloc only once one big memory size if the threads are sharing the buffer
  int buffer_size = inp_static_size*inp_type_mem/sizeof(TYPE);
  if (VECTOR == 1)
    buffer_size = inp_static_size*inp_type_mem/sizeof(__m128i);
  if(inp_ubuffer == 0 && (inp_allocmode==2 || inp_allocmode==4) && VECTOR==0)
    mainBuffer = allocation(buffer_size);
  if(inp_ubuffer == 0 && (inp_allocmode==2 || inp_allocmode==4) && VECTOR==1)
    vmainBuffer = allocation128(buffer_size);
  
  // Child threads (workers)
  pthread_t threads[n_thr];
  struct thread_data temp[n_thr];
  thread_data_array=temp;
  int rc, t;
  // Initialize mutex and condition variable objects 
  thr_counter=0;
  kill_threads = FALSE;
  pthread_mutex_init(&count_mutex, NULL);
  pthread_cond_init(&count_cond, NULL);
  pthread_cond_init(&finished_cond, NULL);
  pthread_mutex_lock(&count_mutex);
  // Creating threads 
  for(t=0; t<n_thr; t++){
    //printf("In main: creating thread %ld\n", t);
    thread_data_array[t].td_id = t;
    thread_data_array[t].td_sum = 0;
    //Linking the buffer(s) to threads
    if (VECTOR == 0 && (inp_allocmode==2 || inp_allocmode==4))
       {
	 if(inp_ubuffer == 0)
	   thread_data_array[t].td_buffer = mainBuffer;
	 else
	   thread_data_array[t].td_buffer = allocation(buffer_size);
       }
     else if (VECTOR == 1 && (inp_allocmode==2 || inp_allocmode==4))
       {
	 if(inp_ubuffer == 0)
	   thread_data_array[t].td_vectbuffer = vmainBuffer;
	 else
	   thread_data_array[t].td_vectbuffer = allocation128(buffer_size);
       }
    rc = pthread_create(&threads[t], NULL, thread_func, (void *)t);
    if (rc){
      printf("ERROR; return code from pthread_create() is %d\n", rc);
      exit(-1);
    }
  }
  //printf("Blocked until threads are ready!\n");
  pthread_cond_wait(&finished_cond, &count_mutex);
  pthread_mutex_unlock(&count_mutex);
  // Executing experiments
  while (fgets(line, 200, fpInput) != NULL)
    {
      sscanf (line, "%d %d %d %d %d %d",&m_num, &m_stride, &m_mem, &m_cycles, &m_mode, &m_exemode);
      measure(fpdat, m_num, m_stride, m_mem, m_cycles, m_mode, m_exemode);
    } 
  
  // Killing the child threads (workers)
  kill_threads = TRUE;
  pthread_cond_broadcast(&count_cond);
  pthread_mutex_unlock(&count_mutex);
  time_end();

  // Printing the output
  printf("ID of this measurement = %d; Total time: %lf\n", result,tot_dif.tv_sec+(double)(tot_dif.tv_nsec/(double)BILLION));
  fprintf(fpdat,"# ID of this measurement = %d; Total time: %lf sec\n", result,tot_dif.tv_sec+(double)(tot_dif.tv_nsec/(double)BILLION));  
  
  // Deallocating buffers, closing files
  if(inp_ubuffer == 0 && (inp_allocmode==2 || inp_allocmode==4) && VECTOR==0)
      free(mainBuffer);
  if(inp_ubuffer == 0 && (inp_allocmode==2 || inp_allocmode==4) && VECTOR==1)
      free(vmainBuffer);
  free(thr_core);
  fclose(fpInput);
  fclose(fpdat);
  
  // Waiting for child threads (workers) to die and then ending
  pthread_exit(NULL);
}

TYPE* allocation(int buffer_size)
{
  TYPE* buffer;
  int i;
  buffer = (TYPE*) malloc (buffer_size*sizeof(TYPE));
  if (buffer==NULL) printf("Error while allocation memory for buffer");
  for(i=0; i<buffer_size; i++)
    {
      buffer[i]=(TYPE)rand();
    }
  return buffer;
}

__m128i* allocation128(int buffer_size)
{
  int i;
  __m128i* vectBuffer;
  vectBuffer = (__m128i*) malloc (buffer_size*sizeof(__m128i));
  if (vectBuffer==NULL) printf("Error while allocation memory for buffer");
  for(i=0; i<buffer_size; i++)
    {
      vectBuffer[i]=_mm_set_epi32(rand(),rand(),rand(),rand());
    }
  return vectBuffer;
}

// Reading kernel parameters from input file
void kernelparameters(char* argv[])
//argv[*]= ParaKInput.dat ParaKData#.dat n_thr thr_core inp_seed inp_allocmode inp_static_size, inp_type_mem, inp_accesses_sqr
{
  int argm=1;
  strcpy(inputfile, argv[argm++]);
  strcpy(outputfile, argv[argm++]);
  n_thr=atoi(argv[argm++]);
  
  thr_core = (int*) malloc(n_thr*sizeof(int));
  main_core = atoi(argv[argm++]);
  int i;
  for(i=0;i<n_thr;i++)
   thr_core[i]=atoi(argv[argm++]);
 
  inp_seed=atoi(argv[argm++]);
  inp_allocmode=atoi(argv[argm++]);
  inp_static_size=atoi(argv[argm++]);
  inp_type_mem=atoi(argv[argm++]);
  inp_accesses_sqr=atoi(argv[argm++]);
  inp_ubuffer=atoi(argv[argm++]);
}

// Pinning the processor core to a specific thread
void pincore(unsigned long int core)
{
  if (core!=99)
    {
      // Pin the CPU core
      cpu_set_t  cpu_set;
      CPU_ZERO(&cpu_set);
      CPU_SET(core,&cpu_set);

      /* first bit to mask 1, core 0 */
      sched_setaffinity(0, sizeof(cpu_set_t), &cpu_set) ;
      /* 0 to indicate current process/thread */ 
      sched_yield(); 
      // Finished pin
    }
}

// Start time for the whole program
void time_start(void)
{
  // Computing time offset
  clock_gettime( CLOCK_REALTIME, &tot_start);
  // clock_gettime( CLOCK_REALTIME, &tot_dif); 
  clock_gettime( CLOCK_REALTIME, &tot_stop);
  diff(&tot_start,&tot_stop,&tot_dif);
  time_offset=((double)tot_dif.tv_nsec);// /2;
  // Start of program
  clock_gettime( CLOCK_REALTIME, &tot_start); 
}

// Stop time for the whole program
void time_end(void)
{
  clock_gettime( CLOCK_REALTIME, &tot_stop);
  diff(&tot_start,&tot_stop,&tot_dif);
}

// Calculating time difference
void diff(struct timespec* start, struct timespec* end, struct timespec* result_time)
{
	if ((end->tv_nsec-start->tv_nsec)<0)
	{
		result_time->tv_sec = end->tv_sec-start->tv_sec-1;
		result_time->tv_nsec = (double)BILLION+end->tv_nsec-start->tv_nsec;
	} else {
		result_time->tv_sec = end->tv_sec-start->tv_sec;
		result_time->tv_nsec = end->tv_nsec-start->tv_nsec;
	}
}

// Central function of "master thread", that one that is doing measurements
void measure(FILE *fpdat, int m_num, int m_stride, int m_mem, int m_cycles, int m_mode, int m_exemode) 
{
  int i,j,t;
  int sum=0;
  int buffer_size = m_mem/sizeof(TYPE);
  if (VECTOR == 1)
    buffer_size = m_mem/sizeof(__m128i);

  // Malloc for every memory size independantly
  if(inp_ubuffer == 0 && (inp_allocmode==1 || inp_allocmode==3) && VECTOR==0)
    mainBuffer = allocation(buffer_size);
  if(inp_ubuffer == 0 && (inp_allocmode==1 || inp_allocmode==3) && VECTOR==1)
    vmainBuffer = allocation128(buffer_size);

  // Choosing starting point when we having a big memory with random access inside it
  int m_start=0;
  if (inp_allocmode==4)
    {
      m_start=rand()%( (inp_static_size*inp_type_mem-m_mem)/sizeof(TYPE));
      if (VECTOR==1)
	m_start=rand()%( (inp_static_size*inp_type_mem-m_mem)/sizeof(__m128i));
      // Aligning m_start
      m_start=m_start-(m_start%256);
    }
  
  // Fixing m_exemode for vectorized instructions
  if(m_exemode==1 && VECTOR==1) 
    m_exemode=7;
  if(m_exemode==2 && VECTOR==1) 
    m_exemode=8;
  
  // Setting up threads
   for(t=0; t<n_thr; t++){
     thread_data_array[t].td_start = m_start;
     thread_data_array[t].td_cycles = m_cycles;
     thread_data_array[t].td_stride = m_stride;
     thread_data_array[t].buffer_size = buffer_size;
     if (VECTOR == 0 && (inp_allocmode==1 || inp_allocmode==3))
       {
	 if(inp_ubuffer == 0)
	   thread_data_array[t].td_buffer = mainBuffer;
	 else
	   thread_data_array[t].td_buffer = allocation(buffer_size);
       }
     else if (VECTOR == 1 && (inp_allocmode==1 || inp_allocmode==3))
       {
	 if(inp_ubuffer == 0)
	   thread_data_array[t].td_vectbuffer = vmainBuffer;
	 else
	   thread_data_array[t].td_vectbuffer = allocation128(buffer_size);
       }
     thread_data_array[t].td_exemode = m_exemode;
     thread_data_array[t].td_mem = m_mem;
   }
   pthread_cond_broadcast(&count_cond);
   pthread_mutex_unlock(&count_mutex);
   // Threads are working, waiting for them to finish
   pthread_cond_wait(&finished_cond, &count_mutex);

   // Printing output for each thread
   for(t=0; t<n_thr; t++)
     {
       result+=thread_data_array[t].td_sum;
       start=thread_data_array[t].td_time_start;
       stop=thread_data_array[t].td_time_stop;
       // End time and computing difference
       diff(&start,&stop,&dif);
       dif.tv_nsec-=time_offset;
  
       // Computing BANDWIDTH
       double BANDWIDTH=0;
       if(m_exemode==3)
	 BANDWIDTH=(double)((m_cycles/1024)*(m_cycles/1024))/(double)(dif.tv_sec+(double)dif.tv_nsec/(double)BILLION);
       else 
	 {
	   if (m_cycles>1024)
	     BANDWIDTH=(double)((m_mem/1024)*(m_cycles/1024))/(double)(dif.tv_sec+(double)dif.tv_nsec/(double)BILLION);
	   else if(m_mem>1024*1024)
	     BANDWIDTH=(double)(((m_mem/1024)/1024)*m_cycles)/(double)(dif.tv_sec+(double)dif.tv_nsec/(double)BILLION);
	   else
	     BANDWIDTH=(double)(((m_mem*m_cycles)/1024)/(double)(dif.tv_sec+(double)dif.tv_nsec/(double)BILLION))/1024;
	 }

       // Writing results into file 
       fprintf(fpdat, "  %d\t %d\t %d\t %d\t %d\t %p  %lf\t %lf\t %lf\t %lf\t %d\t %d\n", m_num, t, thr_core[t],m_stride, m_mem, &mainBuffer[m_start], start.tv_sec+(double)(start.tv_nsec/(double)BILLION), stop.tv_sec+(double)(stop.tv_nsec/(double)BILLION), dif.tv_sec+(double)(dif.tv_nsec/(double)BILLION), BANDWIDTH, m_mode, m_exemode);
  
     }
   // Deallocating buffer in case memory is allocated for each measurement
  if(inp_allocmode==1 && VECTOR==0) free(mainBuffer);
  if(inp_allocmode==1 && VECTOR==1) free(vmainBuffer);
}

// Thread function, code that workers execute 
void *thread_func(void *threadid)
{
  // Getting thread ID (equal to thread_data_array[#].td_id)
  int tid = (int)threadid;
  //printf("Hello World! It's me, thread #%ld on core: %d\n", tid,thr_core[tid]);   
  // Pinning the processor core to this thread
  pincore(thr_core[tid]);
   
  // Thread is ready, now waiting on barrier
  pthread_mutex_lock(&count_mutex);
  thr_counter++;
  if(thr_counter==n_thr){
    thr_counter=0; 
    pthread_cond_signal(&finished_cond);
    pthread_cond_wait(&count_cond, &count_mutex);
  }    
  else
    pthread_cond_wait(&count_cond, &count_mutex);
  pthread_mutex_unlock(&count_mutex);
   
  while(!kill_threads)
    {
      // Executing the small kernel 
      clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_start);
      // Choosing execution mode and executing for loop
      switch( thread_data_array[tid].td_exemode )
	{
	case 1:thread_data_array[tid].td_sum+=exemode1(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size, &thread_data_array[tid].td_buffer); clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop); break;
	case 2:thread_data_array[tid].td_sum+=exemode2(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size, &thread_data_array[tid].td_buffer); clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop); break;
	case 7:thread_data_array[tid].td_sum+=exemode7(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size,&thread_data_array[tid].td_vectbuffer, tid); break;
	case 8:thread_data_array[tid].td_sum+=exemode8(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size,&thread_data_array[tid].td_vectbuffer, tid); break;
      //For ARM code comment from here
	  /*
	case 9:thread_data_array[tid].td_sum+=exemode9(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size,thread_data_array[tid].td_mem); break;
	case 10:thread_data_array[tid].td_sum+=exemode10(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size,thread_data_array[tid].td_mem); break;
	case 11:thread_data_array[tid].td_sum+=exemode11(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size,thread_data_array[tid].td_mem); break;
	  */
	case 0:break;
	default :thread_data_array[tid].td_sum+=exemode1(thread_data_array[tid].td_start,thread_data_array[tid].td_cycles,thread_data_array[tid].td_stride,thread_data_array[tid].buffer_size, thread_data_array[tid].td_buffer); clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop); break;
	}
   
      // Finished task, now waiting on barrier
      pthread_mutex_lock(&count_mutex);
      thr_counter++;
      if(thr_counter==n_thr){
	thr_counter=0; 
	pthread_cond_signal(&finished_cond);
	pthread_cond_wait(&count_cond, &count_mutex);
      }    
      else
	pthread_cond_wait(&count_cond, &count_mutex);   
      pthread_mutex_unlock(&count_mutex);
    }
   
  // Thread has finished its job
  //printf("Thread #%ld finished!\n", tid);
  pthread_exit(NULL);
}

// ##############################################
// Different small kernels

 //Standard execution  
int exemode1(int m_start, int m_cycles, int m_stride, int buffer_size, TYPE* buffer)
{
  register int i,j,sum=0;

  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride)
	{
	  sum+=buffer[j];
	}
    }
  return sum;
}

//Correct loop unrolling
int exemode2(int m_start, int m_cycles, int m_stride, int buffer_size, TYPE* buffer)
{
  register int i,j,sum=0;
  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride*8)
	{
	  sum+=buffer[j];
	  sum+=buffer[j+m_stride];
	  sum+=buffer[j+2*m_stride];
	  sum+=buffer[j+3*m_stride];
	  sum+=buffer[j+4*m_stride];
	  sum+=buffer[j+5*m_stride];
	  sum+=buffer[j+6*m_stride];
	  sum+=buffer[j+7*m_stride];
	}
    }
  return sum;
}

//Here starts the Intel vectorized code, comment from here
 //Vectorized instructions standard execution
int exemode7(int m_start, int m_cycles, int m_stride, int buffer_size, __m128i* vectBuffer, int tid)
{
  //Variables
  register int i,j;
  long long resultV[2];
  __m128i sumV=_mm_set_epi32(0,0,0,0);
  
  //Start time
  clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_start);

  //Kernel
  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride)
	{
	  sumV=_mm_add_epi64(sumV,vectBuffer[j]);
	}
    }

  //End time and computing difference
  clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop);

  //Computing the result
  __m128i* p;
  p = (__m128i *) resultV;
  _mm_store_si128 (p, sumV);  
  return (int) (resultV[0]+resultV[1]);
}


 //Vectorized instructions with loop unrolling
int exemode8(int m_start, int m_cycles, int m_stride, int buffer_size, __m128i* vectBuffer, int tid)
{
  //Variables
  register int i,j;
  long long resultV[2];
  //###############3
  //  int buffer_size = m_mem/sizeof(__m128i);
  //__m128i* vectBuffer = (__m128i*) malloc (buffer_size*sizeof(__m128i));
  //if (vectBuffer==NULL) printf("Error while allocation memory for buffer");
  //for(i=0; i<buffer_size; i++)
  // {
  //   vectBuffer[i]=_mm_set_epi32(rand(),rand(),rand(),rand());
  // }
  // ###############
  __m128i sumV=_mm_set_epi32(0,0,0,0);

  //Start time
clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_start);
      
  //Kernel
  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride*8)
	{
	  sumV=_mm_add_epi64(sumV,vectBuffer[j]);
	  sumV=_mm_add_epi64(sumV,vectBuffer[j+m_stride]);
	  sumV=_mm_add_epi64(sumV,vectBuffer[j+m_stride*2]);
	  sumV=_mm_add_epi64(sumV,vectBuffer[j+m_stride*3]);
	  sumV=_mm_add_epi64(sumV,vectBuffer[j+m_stride*4]);
	  sumV=_mm_add_epi64(sumV,vectBuffer[j+m_stride*5]);
	  sumV=_mm_add_epi64(sumV,vectBuffer[j+m_stride*6]);
	  sumV=_mm_add_epi64(sumV,vectBuffer[j+m_stride*7]);
	}
    }
      
  //End time and computing difference
clock_gettime( CLOCK_REALTIME, &thread_data_array[tid].td_time_stop);

  //Computing the result
  __m128i* p;
  p = (__m128i *) resultV;
  _mm_store_si128 (p, sumV);  
  //free(vectBuffer);      
  
  return (int) (resultV[0]+resultV[1]);
}

 //AVX Vectorized instructions standard execution
// Need to be rewritten
int exemode9(int m_start, int m_cycles, int m_stride, int buffer_size,int m_mem)
{
  //Variables
  register int i,j;
  double resultV[4];
  __m256d* vectBuffer;
  buffer_size = m_mem/sizeof(__m256d);
  
  //Initialization
  //vectBuffer = (__m256d*) malloc (buffer_size*sizeof(__m256d));
  posix_memalign((void**)&vectBuffer,256,buffer_size*sizeof(__m256d));
  if (vectBuffer==NULL) printf("Error while allocation memory for buffer");
  for(i=0; i<buffer_size; i++)
    {
      vectBuffer[i]=_mm256_set1_pd(1.);
      }
  
  __m256d sumV=_mm256_set1_pd(0);
  
  //Start time
  clock_gettime( CLOCK_REALTIME, &start);
    
  //Kernel
  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride)
	{
	  sumV=_mm256_add_pd(sumV,vectBuffer[j]);
	}
    }
  
  //End time and computing difference
  clock_gettime( CLOCK_REALTIME, &stop);

  //Computing the result
  __m256d* p;
  p = (__m256d *) resultV;
  _mm256_store_pd(p, sumV);  
  free(vectBuffer);
  return (int) (resultV[0]+resultV[1]+resultV[2]+resultV[3]);
}

 //AVX Vectorized instructions with loop unrolling
// Need to be rewritten
int exemode10(int m_start, int m_cycles, int m_stride, int buffer_size,int m_mem)
{
  //Variables
  register int i,j;
  double resultV[4];
  __m256d* vectBuffer;

  //Initialization
 posix_memalign((void**)&vectBuffer,256,buffer_size*sizeof(__m256d));
    
  if (vectBuffer==NULL) printf("Error while allocation memory for buffer");
  for(i=0; i<buffer_size; i++)
    {
      vectBuffer[i]=_mm256_set_pd(1.,2.,3.,4);
      }
  __m256d sumV=_mm256_set1_pd(0);

  __m256d sumV1=_mm256_set1_pd(0);
  __m256d sumV2=_mm256_set1_pd(0);
  __m256d sumV3=_mm256_set1_pd(0);
  __m256d sumV4=_mm256_set1_pd(0);
  __m256d sumV5=_mm256_set1_pd(0);
  __m256d sumV6=_mm256_set1_pd(0);
  __m256d sumV7=_mm256_set1_pd(0);
  __m256d sumV8=_mm256_set1_pd(0);
 
  //Start time
  clock_gettime( CLOCK_REALTIME, &start);
  
  //Kernel
  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride*4)
	{
	  sumV1=_mm256_add_pd(sumV1,vectBuffer[j]);
	  sumV2=_mm256_add_pd(sumV2,vectBuffer[j+m_stride]);
	  sumV3=_mm256_add_pd(sumV3,vectBuffer[j+m_stride*2]);
	  sumV4=_mm256_add_pd(sumV4,vectBuffer[j+m_stride*3]);
	  sumV5=_mm256_add_pd(sumV5,vectBuffer[j+m_stride*4]);
	  sumV6=_mm256_add_pd(sumV6,vectBuffer[j+m_stride*5]);
	  sumV7=_mm256_add_pd(sumV7,vectBuffer[j+m_stride*6]);
	  sumV8=_mm256_add_pd(sumV8,vectBuffer[j+m_stride*7]);
	}
    }

  //End time and computing difference
  clock_gettime( CLOCK_REALTIME, &stop);

  sumV=_mm256_add_pd(sumV,sumV1);
  sumV=_mm256_add_pd(sumV,sumV2);  
  sumV=_mm256_add_pd(sumV,sumV3);
  sumV=_mm256_add_pd(sumV,sumV4);
  sumV=_mm256_add_pd(sumV,sumV5);
  sumV=_mm256_add_pd(sumV,sumV6);  
  sumV=_mm256_add_pd(sumV,sumV7);
  sumV=_mm256_add_pd(sumV,sumV8);
  

  //Computing the result
  __m256d* p;
  p = (__m256d *) resultV;
  _mm256_store_pd(p, sumV);  
  free(vectBuffer);
  return (int) (resultV[0]+resultV[1]+resultV[2]+resultV[3]);
}

 //Standard execution with double
int exemode11(int m_start, int m_cycles, int m_stride, int buffer_size,int m_mem)
{
  //Variables
  register int i,j;
  double* vectBuffer;

  //Initialization
  vectBuffer = (double*) malloc (buffer_size*sizeof(double));
  if (vectBuffer==NULL) printf("Error while allocation memory for buffer");
  for(i=0; i<buffer_size; i++)
    {
      vectBuffer[i]=1.;
    }
  double sumV=0;
 
  //Start time
  clock_gettime( CLOCK_REALTIME, &start);
  
  //Kernel
  for(i=0; i<m_cycles*m_stride; i++)
    {
      for(j=m_start;j<buffer_size+m_start;j+=m_stride)
	{
	  sumV+=vectBuffer[j];
	}
    }
  
  //End time and computing difference
  clock_gettime( CLOCK_REALTIME, &stop);

  //Computing the result 
  return (int) sumV;
}


//Here starts the ARM code, comment up until here

//ARM Vectorized instructions with loop unrolling
/*
int exemode7(int START, int CYCLES, int STRIDE, int buffer_size,int MEM)
{
//Variables
  int i,j;
  int64_t resultV[2];
  int64x2_t* vectBuffer;

  //Initialization
  vectBuffer = (int64x2_t*) malloc (buffer_size*sizeof(int64x2_t));
  if (vectBuffer==NULL) printf("Error while allocation memory for buffer");
  for(i=0; i<buffer_size; i++)
    {
      vectBuffer[i]=vmovq_n_s64(rand());
    }
  int64x2_t sumV=vmovq_n_s64(0);
 
  //Start time
  clock_gettime( CLOCK_REALTIME, &start);

  //Kernel
  for(i=0; i<CYCLES*STRIDE; i++)
    {
      for(j=START;j<buffer_size+START;j+=STRIDE)
	{
	  sumV=vaddq_s64(sumV,vectBuffer[j]);
	}
    }

  //End time and computing difference
  clock_gettime( CLOCK_REALTIME, &stop);

  //Computing the result 
   vst1q_s64 (resultV, sumV);

  return (int) (resultV[0]+resultV[1]);
}
 //ARM Vectorized instructions with loop unrolling
int exemode8(int START, int CYCLES, int STRIDE, int buffer_size,int MEM)
{
//Variables
  int i,j;
  int64_t resultV[2];
  int64x2_t* vectBuffer;

  //Initialization
  vectBuffer = (int64x2_t*) malloc (buffer_size*sizeof(int64x2_t));
  if (vectBuffer==NULL) printf("Error while allocation memory for buffer");
  for(i=0; i<buffer_size; i++)
    {
      vectBuffer[i]=vmovq_n_s64(rand());
    }
  int64x2_t sumV=vmovq_n_s64(0);
 
  //Start time
  clock_gettime( CLOCK_REALTIME, &start);

  //Kernel
  for(i=0; i<CYCLES*STRIDE; i++)
    {
      for(j=START;j<buffer_size+START;j+=8*STRIDE)
	{
	  sumV=vaddq_s64(sumV,vectBuffer[j]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+STRIDE]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+2*STRIDE]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+3*STRIDE]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+4*STRIDE]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+5*STRIDE]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+6*STRIDE]);
	  sumV=vaddq_s64(sumV,vectBuffer[j+7*STRIDE]); 
	}
    }

  //End time and computing difference
  clock_gettime( CLOCK_REALTIME, &stop);

  //Computing the result 
   vst1q_s64 (resultV, sumV);

  return (int) (resultV[0]+resultV[1]);
}
*/
