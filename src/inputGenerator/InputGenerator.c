// This is a input generator for Snow
// This code should produce input file for kernel

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define swap(type, i, j) {type t = i; i = j; j = t;}

int SEED, N_ARRAYS, N_REP, N_CYCLES, MIN_MEM, MAX_MEM, TYPE_MEM, MIN_STRIDE, MAX_STRIDE, MODE;
int GCC, CORE, ALLOCTYPE, SEEDK, EXEMODE, ACCESSES_SQR, UBUFFER, WARMUP, ALLOCMODE, STATIC_SIZE, TYPE_MEMK;
char line[200];
int length=0;
int counter=0;
int memfix[200];
int stridefix[200];

void getParameters(FILE* fp);
void getParametersKernel(FILE* fp);
void writeKernelParameters(FILE* fpdat);
void produceInputReg(FILE* fpdat);
void produceInputInv(FILE* fpdat);
void produceInputPerm(FILE* fpdat);
void produceInputFix(FILE* fpdat);
void produceInputStrides(FILE* fpdat);
void produceRandom(FILE* fpdat);

int main(int argc, char* argv[])
//argv[1]=Parameters.txt Input file with parameters
//argv[2]=Snow2Input.dat   Output file 
{
  FILE *fp;
  fp=fopen(argv[1], "r");
  if (fp==NULL) printf("Error while opening the parameters file"); 
  getParameters(fp);
  getParametersKernel(fp); 
  fclose(fp);
  
  FILE *fpdat;
  fpdat=fopen(argv[2], "a+");
  if (fpdat==NULL) printf("Error while opening the .dat file");
  writeKernelParameters(fpdat);

  switch( MODE )
     {
     case 1  : produceInputReg(fpdat); break;
     case 2  : produceInputInv(fpdat); break;
     case 3  : produceInputPerm(fpdat); break;
     case 123:  produceInputReg(fpdat); produceInputInv(fpdat); produceInputPerm(fpdat); break;
     case 4  : produceInputFix(fpdat); break;
     case 5  : produceInputStrides(fpdat); break;
     case 6  : produceRandom(fpdat); break;
     default : produceInputReg(fpdat); break;
     }

  fclose(fpdat);
  return 0;
}

//Producing Ex2Input.dat file in regular order of FOR loops
void produceInputReg(FILE* fpdat)
{  
  int REP, S_MEM, S_STRIDE;  
  for(REP=1;REP<=N_REP;REP++)
    {
      for(S_STRIDE = MIN_STRIDE; S_STRIDE<=MAX_STRIDE; S_STRIDE=S_STRIDE*2)
      {
	for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1)
	  {
	    fprintf(fpdat,"%d %d %d %d 1 %d\n",counter++, S_STRIDE, S_MEM*N_ARRAYS*TYPE_MEM, N_CYCLES, EXEMODE);
	  }
      }
    }
}

//Producing Ex2Input.dat file in inverted order of FOR loops
void produceInputInv(FILE* fpdat)
{  
  int REP, S_MEM, S_STRIDE;
  
  for(REP=1;REP<=N_REP;REP++)
    {
      for(S_STRIDE = MAX_STRIDE; S_STRIDE>=MIN_STRIDE; S_STRIDE=S_STRIDE/2)
      {
	for(S_MEM = MAX_MEM; S_MEM>=MIN_MEM; S_MEM=S_MEM-1)
	  {
	    fprintf(fpdat,"%d %d %d %d 2 %d\n",counter++, S_STRIDE, S_MEM*N_ARRAYS*TYPE_MEM, N_CYCLES, EXEMODE);
	  }
      }
    }
}

//Producing Ex2Input.dat file with a random permutation of S_MEM 
void produceInputPerm(FILE* fpdat)
{
  int REP, S_MEM, S_STRIDE, i;
  srand(SEED);
  int list[MAX_MEM-MIN_MEM+1];
  
  for(REP=1;REP<=N_REP;REP++)
    {
      //PERMUTATIONS
      for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1)
	{
	  list[S_MEM-MIN_MEM]=S_MEM;	 
	}
      //Testing for bugs
      /*for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1)
      	  {
      	    printf("array= %d, value= %d\n", S_MEM-MIN_MEM, list[S_MEM-MIN_MEM]);
      	  }
	  printf("PRINT\n");*/
      for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1)
	{
	  i=rand()%(MAX_MEM-MIN_MEM+1);
	  swap(int, list[S_MEM-MIN_MEM], list[i]);
	  //swap(int, list[S_MEM-MIN_MEM], list[rand() % (MAX_MEM-MIN_MEM+1)]);
	}
      //Testing for bugs
      /* for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1) 
      	  {
      	    printf("array= %d, value= %d\n", S_MEM-MIN_MEM, list[S_MEM-MIN_MEM]);
	    } */
      
      //INPUT GENERATION
      for(S_STRIDE = MIN_STRIDE; S_STRIDE<=MAX_STRIDE; S_STRIDE=S_STRIDE*2)
	{
	  for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1)
	    {
	      fprintf(fpdat,"%d %d %d %d 3 %d\n",counter++, S_STRIDE, list[S_MEM-MIN_MEM]*N_ARRAYS*TYPE_MEM, N_CYCLES, EXEMODE);
	    }
	}
    }
}

//Producing Ex2Input.dat file in fixed order of FOR loops
void produceInputFix(FILE* fpdat)
{
  int REP, S_STRIDE, i;
  //Testing for bugs for(i = 0; i<length; i++) printf("%d \n", memfix[i]);

  for(REP=1;REP<=N_REP;REP++)
    {
      for(S_STRIDE = MIN_STRIDE; S_STRIDE<=MAX_STRIDE; S_STRIDE=S_STRIDE*2)
      {	
	for(i = 0; i<length; i++)
	  {
	    fprintf(fpdat,"%d %d %d %d 4 %d\n", counter++, S_STRIDE, memfix[i]*N_ARRAYS*TYPE_MEM, N_CYCLES, EXEMODE);
	    //Testing for bugs printf("%d %d %d %d 4\n", S_STRIDE, memfix[i]*N_ARRAYS*TYPE_MEM, N_CYCLES);
	  }
      }
    }
}

//Producing Ex2Input.dat file with a random permutation of S_MEM 
void produceInputStrides(FILE* fpdat)
{  
  int REP, S_MEM, i, j;
  srand(SEED);
  int list[MAX_MEM-MIN_MEM+1];
  
  for(REP=1;REP<=N_REP;REP++)
    {
      //PERMUTATIONS
      for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1)
	{
	  list[S_MEM-MIN_MEM]=S_MEM;	 
	}
      //Testing for bugs
      /*for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1)
      	  {
      	    printf("array= %d, value= %d\n", S_MEM-MIN_MEM, list[S_MEM-MIN_MEM]);
      	  }
	  printf("PRINT\n");*/
      for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1)
	{
	  i=rand()%(MAX_MEM-MIN_MEM+1);
	  swap(int, list[S_MEM-MIN_MEM], list[i]);
	  //swap(int, list[S_MEM-MIN_MEM], list[rand() % (MAX_MEM-MIN_MEM+1)]);
	}
      //Testing for bugs
      /* for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1) 
      	  {
      	    printf("array= %d, value= %d\n", S_MEM-MIN_MEM, list[S_MEM-MIN_MEM]);
	    } */
      
      //INPUT GENERATION
      for(j=0;j<length;j++)
	{
	  for(S_MEM = MIN_MEM; S_MEM<=MAX_MEM; S_MEM=S_MEM+1)
	    {
	      fprintf(fpdat,"%d %d %d %d 5 %d\n",counter++, stridefix[j], list[S_MEM-MIN_MEM]*N_ARRAYS*TYPE_MEM, N_CYCLES, EXEMODE);
	    }
	}
    }
}

// Randomly choosen 
void produceRandom(FILE* fpdat)
{  
  int REP, S_MEM, S_STRIDE, i;  
  srand(SEED);

  for(REP=1;REP<=N_REP;REP++)
    {
      for(S_STRIDE = MIN_STRIDE; S_STRIDE<=MAX_STRIDE; S_STRIDE=S_STRIDE*2)
      {
	for(i = 0; i<(MAX_MEM-MIN_MEM)/5; i++)
	  {
	    S_MEM = (rand()%(MAX_MEM-MIN_MEM))+MIN_MEM;
	    fprintf(fpdat,"%d %d %d %d 6 %d\n",counter++, S_STRIDE, S_MEM*N_ARRAYS*TYPE_MEM, N_CYCLES, EXEMODE);
	  }
      }
    }
}

//Reading Parameters.txt
void getParameters(FILE* fp)
{
  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  while(line[0] == '#')
     if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  // The order: N_ARRAYS, N_REP, N_CYCLES, SEED, MIN_MEM, MAX_MEM, MIN_STRIDE, MAX_STRIDE, MODE;
  sscanf (line, "%d", &SEED);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
  else  sscanf (line, "%d", &N_ARRAYS);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
  else  sscanf (line, "%d", &N_REP);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else  sscanf (line, "%d", &N_CYCLES);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else  sscanf (line, "%d", &MIN_MEM);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else  sscanf (line, "%d", &MAX_MEM);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else  sscanf (line, "%d", &TYPE_MEM);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else  sscanf (line, "%d", &MIN_STRIDE);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else  sscanf (line, "%d", &MAX_STRIDE);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else  sscanf (line, "%d", &MODE);
  

  //Additional parameters for MODE=4 || MODE=5
 if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 

  if (MODE==4)
    {
      int k;
       fscanf(fp, "%d",&k);
       if(k==0) printf("Error, Paramater.txt not initialized correctly"); 
       while(k!=0)
	 {
	   memfix[length++]=k;
	   fscanf(fp, "%d",&k);	   
	 }
       //Testing for bugs: int i;for( i = 0; i<length; i++) printf("%d \n", memfix[i]);
    }
  else if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
   
  if (MODE==5)
    {
      int k;
      fscanf(fp, "%d",&k);
      if(k==0) printf("Error, Paramater.txt not initialized correctly"); 
      while(k!=0)
	{
	  stridefix[length++]=k;
	  fscanf(fp, "%d",&k);	   
	}
      //Testing for bugs: int i; for(i = 0; i<length; i++) printf("%d \n", stridefix[i]);
    } 
}

void getParametersKernel(FILE* fp)
{ 
  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  while(line[0] != '!')
     if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  // The order: GCC, CORE, SEED, ALLOCMODE, STATIC_SIZE, EXEMODE;
  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else sscanf (line, "%d", &GCC);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else sscanf (line, "%d", &CORE);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else sscanf (line, "%d", &ALLOCTYPE);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file"); 
  else sscanf (line, "%d", &SEEDK);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
  else  sscanf (line, "%d", &EXEMODE);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
  else  sscanf (line, "%d", &UBUFFER);

if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
  else  sscanf (line, "%d", &WARMUP);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
      else  sscanf (line, "%d", &ACCESSES_SQR);

  if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
  else  sscanf (line, "%d", &ALLOCMODE);

  if(ALLOCMODE==2 || ALLOCMODE==4)
    {
      if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
      else  sscanf (line, "%d", &STATIC_SIZE);
      if (fgets(line, 200, fp) == NULL) printf("Error while reading the parameters file");
      else  sscanf (line, "%d", &TYPE_MEMK);
    }
}

void writeKernelParameters(FILE* fpdat)
{
  fprintf(fpdat,"!GCC %d\n!CORE %d\n!ALLOCTYPE %d\n", GCC, CORE, ALLOCTYPE);

  fprintf(fpdat,"!SEEDK %d\n!ALLOCMODE %d\n", SEEDK, ALLOCMODE);

  if(ALLOCMODE==2 || ALLOCMODE==4)
    fprintf(fpdat,"!STATIC_SIZE %d\n!TYPE_MEM %d\n", STATIC_SIZE, TYPE_MEMK);
  else
    fprintf(fpdat,"!STATIC_SIZE 0\n!TYPE_MEM 0\n");
  if(EXEMODE==3)
    fprintf(fpdat,"!ACCESSES_SQR %d\n", ACCESSES_SQR);
  else
    fprintf(fpdat,"!ACCESSES 0\n");

  fprintf(fpdat,"!UBUFFER %d\n", UBUFFER);
  fprintf(fpdat,"!UBUFFER %d\n", WARMUP);
  fprintf(fpdat,"!\n");
}
