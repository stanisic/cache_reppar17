#!/bin/bash
# Script for producing .pdf from data files

# Parameters
starting_index=0
overall=11
datafolder="dataSnowMalloc1"
datafile="ParaKData"
data_analysis="data_analysis"
analysis_pdf="ParaK"

# Possibility to use command line arguments
param1=$1
param2=$2
if [[ $# == 2 ]]; then
    starting_index=$param1
    overall=$param2
fi

# Starting the pdf production
index=$starting_index
while [ $index -le $overall ] ; do
    cp -r analysis ${datafolder}/${data_analysis}${index}
    grep '#' ${datafolder}/$datafile${index}.dat > ${datafolder}/${data_analysis}${index}/TempParameters.dat
    grep -v '#' ${datafolder}/$datafile${index}.dat | sed -e 's/  */ /g' -e 's/://g' | cut -d ' ' -f 2,3,4,5,6,10,11 > ${datafolder}/${data_analysis}${index}/Temp.dat
    cd ${datafolder}/${data_analysis}${index}/
    make &
    cd ..
    cd ..
    echo "STARTED ${index}/${overall}!"
    index=`expr $index + 1`
done

# Waiting for child processes to finish
wait

# Copying .pdfs near to data and cleanup
index=$starting_index
while [ $index -le $overall ] ; do
    output=${datafile}${index}
    cp ${datafolder}/${data_analysis}${index}/${analysis_pdf}.pdf ${datafolder}/${output}.pdf
    rm -rf ${datafolder}/${data_analysis}${index}
    index=`expr $index + 1`
    echo ${output}
done

