#!/bin/bash
# Script for producing .pdf from data files

# Parameters
index=9
overall=9
datafolder="data"
datafile="ParaKData"

# Possibility to use command line arguments
param1=$1
param2=$2
if [[ $# == 2 ]]; then
    index=$param1
    overall=$param2
fi

while [ $index -le $overall ] ; do
    grep '#' ${datafolder}/$datafile${index}.dat > analysis/TempParameters.dat
    grep -v '#' ${datafolder}/$datafile${index}.dat | sed -e 's/  */ /g' -e 's/://g' | cut -d ' ' -f 2,3,4,5,6,10,11 > analysis/Temp.dat
    cd analysis/
    make
    cd ..
    cp analysis/ParaK.pdf ${datafolder}/$datafile${index}.pdf
    echo "PRODUCING PDF ${index}/${overall} IS DONE!"
    index=`expr $index + 1`
done

