%%
\documentclass{article}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{subfigure}
\usepackage{figlatex}
\usepackage{commath}
\usepackage{multirow}

\begin{document}

\section{ANALYSIS-L3 zoomed}

Files that are used: \\
dataPaw2UBuff2/ParaKData29.dat \\
dataPaw2UBuff2/ParaKData30.dat \\
dataPaw2UBuff2/ParaKData31.dat \\
dataPaw2UBuff2/ParaKData32.dat \\

Two threads are executing on different cores (and main thread that is blocked on the third one)
\\
42 measurement is done for each memory size and they have been all completely randomized
\\
Results from only one thread is presented, since both threads have similar results
\\
We are using vectorized instruction (128b) with loop unrolling and gcc=-03 which is giving the best performance

%\verbatiminput{TempParameters.dat}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 

<<setup,include=FALSE>>=
opts_chunk$set(cache=TRUE,dpi=300,echo=FALSE)
require(ggplot2)
require(plyr)

line_stat <- function(fun, geom="point", ...) { stat_summary(fun.y=fun, colour="black", geom=geom, size = 1.2, ...) } 
cap_name <- function(num){
  output <- "STRIDE = ";
  output <- paste(output,num,sep="")
  return(output)
}

#Execution priority
con <- file("TempParameters.dat", "rt")
line <- scan(con,"",20)
prmode <- line[13]
prnum <- line[14]
prmode <- if(prmode=="normal") "NORMAL" else
          if(prmode=="chrt")   "CHRT" else
          if(prmode=="nice")   "NICE" else "/"

#Reading file 
read_file <- function(file,num) {
  dataf <- read.table(file, header=T)
  #Only thread 0
  dataf <- dataf[dataf$THREAD==0,]
  dataf$SIZE <- dataf$SIZE/1024
  #Only bigger size
  dataf <- dataf[dataf$SIZE>100,]
  dataf$PRIORITY <- apply(dataf,1,function(row) prmode)
  dataf$ORIGIN <- num
  return(dataf)
}
#Reading files and binding
dataf <- read_file('TempComp1.dat','SHARED')
dataf <- rbind(dataf,read_file('TempComp2.dat','PRIVATE'))
dataf$ORIGIN <- factor(dataf$ORIGIN, levels = c("SHARED","PRIVATE"))
  
attach(dataf)
Bi <- 1/BANDWIDTH
L1cache <- 32
L2cache <- 256
cache_line <- if(sum(SIZE==L1cache)!=0) 10 else 0
cache_line <- cache_line + if(sum(SIZE==L2cache)!=0) 2 else 0
fixY <- TRUE
fixYMIN <- 0
fixYMAX <- 60000

summary(aov(TIME~STRIDE*SIZE,data=dataf))
summary(lm(TIME~STRIDE*SIZE,data=dataf))
@

Comparing execution for big buffers using one buffer shared between the threads or private buffers for each thread. \Sexpr{cap_name(min(STRIDE))}
\\
Using \emph{ALLOCMODE==4} meaning that one huge memory chunk is allocated in the beginning and later we are accessing randomly inside it.

\begin{figure}[ht!]
\begin{center}
	<<label=L2plot0,echo=FALSE,include=TRUE,cache=TRUE,dev='png',fig.width=6,fig.height=6>>=
	p <- ggplot(data = dataf[STRIDE==min(STRIDE),], aes(SIZE, BANDWIDTH, group = round_any(SIZE, 1, floor))) + geom_boxplot(aes(fill = factor(SIZE)), outlier.size = 0)
p <- p  + scale_x_continuous("MEMORY SIZE (KB)", trans="log2")  
p <- p  + scale_y_continuous("BANDWIDTH (MB/s)",limits=c(fixYMIN, fixYMAX))
p <- p + facet_wrap( ~ ORIGIN,ncol=2) 
  p
	garbage <- dev.off() 
	@	
\end{center}
\caption{\Sexpr{cap_name(min(STRIDE))}}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 

Comparing execution for big buffers using one buffer shared between the threads or private buffers for each thread. \Sexpr{cap_name(max(STRIDE))}
\\
Using \emph{ALLOCMODE==4} meaning that one huge memory chunk is allocated in the beginning and later we are accessing randomly inside it.

\begin{figure}[ht!]
\begin{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 
	<<label=L2plot1,echo=FALSE,include=TRUE,cache=TRUE,dev='png',fig.width=6,fig.height=6>>=
	p <- ggplot(data = dataf[STRIDE==max(STRIDE),], aes(SIZE, BANDWIDTH, group = round_any(SIZE, 1, floor))) + geom_boxplot(aes(fill = factor(SIZE)), outlier.size = 0)
p <- p  + scale_x_continuous("MEMORY SIZE (KB)", trans="log2")  
p <- p  + scale_y_continuous("BANDWIDTH (MB/s)",limits=c(fixYMIN, fixYMAX))
p <- p + facet_wrap( ~ ORIGIN,ncol=2) 
  p
	garbage <- dev.off() 
	@	
\end{center}
\caption{\Sexpr{cap_name(max(STRIDE))}}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 
This is left here just for ggplot2 testing

\begin{figure}[ht!]
\begin{center}
	<<label=L2plotdep1,echo=FALSE,include=FALSE,cache=TRUE,dev='png',fig.width=4,fig.height=6>>=
	p <- ggplot(data = dataf[STRIDE==max(STRIDE) & ORIGIN=='SHARED',], aes(SIZE,	BANDWIDTH, group = round_any(SIZE, 1, floor))) + geom_boxplot(aes(fill = factor(SIZE)),outlier.size = 0)
p <- p +  
scale_x_continuous("MEMORY SIZE (KB)", trans="log2")
p <- p + scale_y_continuous("BANDWIDTH (MB/s)",limits=c(fixYMIN, fixYMAX))
  p
	garbage <- dev.off() 
	@

\mbox{
\subfigure[Using shared buffer\label{shared.buff}]{\includegraphics[width=.49\linewidth]{figure/L2plotdep1.png}}%  
\quad
   
<<label=L2plotdep2,echo=FALSE,include=FALSE,cache=TRUE,dev='png',fig.width=4,fig.height=6>>=
	p <- ggplot(data = dataf[STRIDE==max(STRIDE) & ORIGIN=='PRIVATE',], aes(SIZE,	BANDWIDTH, group = round_any(SIZE, 1, floor))) + geom_boxplot(aes(fill = factor(SIZE)), outlier.size = 0)
p <- p +  
scale_x_continuous("MEMORY SIZE (KB)", trans="log2")
p <- p + scale_y_continuous(name="",limits=c(fixYMIN, fixYMAX))
p <- p + theme(legend.position="none")
  p
	garbage <- dev.off() 
	@	

\subfigure[Using private buffers\label{private.buff}]{\includegraphics[width=.49\linewidth]{figure/L2plotdep2.png}}
}
\end{center}
\caption{\Sexpr{cap_name(max(STRIDE))}}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Changing input files

<<setup2,include=FALSE>>=
#Reading files and binding
detach(dataf)

dataf <- read_file('TempComp3.dat','SHARED')
dataf <- rbind(dataf,read_file('TempComp4.dat','PRIVATE'))
dataf$ORIGIN <- factor(dataf$ORIGIN, levels = c("SHARED","PRIVATE"))
  
attach(dataf)
@


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 

Comparing execution for big buffers using one buffer shared between the threads or private buffers for each thread. \Sexpr{cap_name(min(STRIDE))}
\\
Using \emph{ALLOCMODE==1} meaning that memory is allocated for each single measurement and latter it is released.

\begin{figure}[ht!]
\begin{center}
	<<label=L2plot2,echo=FALSE,include=TRUE,cache=TRUE,dev='png',fig.width=6,fig.height=6>>=
	p <- ggplot(data = dataf[STRIDE==min(STRIDE),], aes(SIZE, BANDWIDTH, group = round_any(SIZE, 1, floor))) + geom_boxplot(aes(fill = factor(SIZE)), outlier.size = 0)
p <- p  + scale_x_continuous("MEMORY SIZE (KB)", trans="log2")  
p <- p  + scale_y_continuous("BANDWIDTH (MB/s)",limits=c(fixYMIN, fixYMAX))
p <- p + facet_wrap( ~ ORIGIN,ncol=2) 
  p
	garbage <- dev.off() 
	@	
\end{center}
\caption{\Sexpr{cap_name(min(STRIDE))}}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 

Comparing execution for big buffers using one buffer shared between the threads or private buffers for each thread. \Sexpr{cap_name(max(STRIDE))}
\\
Using \emph{ALLOCMODE==1} meaning that memory is allocated for each single measurement and latter it is released.

\begin{figure}[ht!]
\begin{center}
	<<label=L2plot3,echo=FALSE,include=TRUE,cache=TRUE,dev='png',fig.width=6,fig.height=6>>=
	p <- ggplot(data = dataf[STRIDE==max(STRIDE),], aes(SIZE, BANDWIDTH, group = round_any(SIZE, 1, floor))) + geom_boxplot(aes(fill = factor(SIZE)), outlier.size = 0)
p <- p  + scale_x_continuous("MEMORY SIZE (KB)", trans="log2")  
p <- p  + scale_y_continuous("BANDWIDTH (MB/s)",limits=c(fixYMIN, fixYMAX))
p <- p + facet_wrap( ~ ORIGIN,ncol=2) 
  p
	garbage <- dev.off() 
	@	
\end{center}
\caption{\Sexpr{cap_name(max(STRIDE))}}
\end{figure}

\end{document}