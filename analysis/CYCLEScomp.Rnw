%%
\documentclass{article}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{subfigure}
\usepackage{figlatex}
\usepackage{commath}
\usepackage{multirow}

\begin{document}

\section{ANALYSIS}

\verbatiminput{TempParameters.dat}


New loading:
\begin{verbatim}
grep '#' data#/ParaKData#.dat > analysis/TempParameters.dat
grep -v '#' data#/ParaKData#.dat | sed -e 's/  */ /g' -e 's/://g' | cut -d ' ' -f 2,3,4,5,6,9,10,12,13 > analysis/Temp.dat
\end{verbatim}

<<setup,include=FALSE>>=
opts_chunk$set(cache=TRUE,dpi=300,echo=FALSE)
require(ggplot2)
require(plyr)

line_stat <- function(fun, geom="point", ...) { stat_summary(fun.y=fun, colour="black", geom=geom, size = 1.2, ...) } 
cap_name <- function(num){
  output <- "STRIDE = ";
  output <- paste(output,num,sep="")
  return(output)
}

#Execution priority
con <- file("TempParameters.dat", "rt")
line <- scan(con,"",20)
prmode <- line[13]
prnum <- line[14]
prmode <- if(prmode=="normal") "NORMAL" else
          if(prmode=="chrt")   "CHRT" else
          if(prmode=="nice")   "NICE" else "/"
#Reading file 
read_file <- function(file,par) {
  dataf <- read.table(file, header=T)
  #Only thread 0
  dataf <- dataf[dataf$THREAD==0,]
  dataf$SIZE <- dataf$SIZE/1024
  #Only STRIDE==1
  dataf <- dataf[dataf$STRIDE==1,]
  dataf$PRIORITY <- apply(dataf,1,function(row) prmode)
  dataf$CYCLES <- par
  return(dataf)
}
#Reading files and binding
dataf <- read_file('TempComp1.dat','N_CYCLES=1024')
dataf <- rbind(dataf,read_file('TempComp2.dat','N_CYCLES=2048'))
dataf <- rbind(dataf,read_file('TempComp3.dat','N_CYCLES=4096'))
dataf <- rbind(dataf,read_file('TempComp4.dat','N_CYCLES=8192'))

dataf$CYCLES <- factor(dataf$ALLOC, levels = c("N_CYCLES=1024","N_CYCLES=2048","N_CYCLES=4096","N_CYCLES=8192"))

attach(dataf)
Bi <- 1/BANDWIDTH
L1cache <- 32
L2cache <- 256
cache_line <- if(sum(SIZE==L1cache)!=0) 10 else 0
cache_line <- cache_line + if(sum(SIZE==L2cache)!=0) 2 else 0
fixY <- FALSE
fixYMIN <- 600
fixYMAX <- 1300

summary(aov(TIME~STRIDE*SIZE,data=dataf))
summary(lm(TIME~STRIDE*SIZE,data=dataf))
@


%ADDED JUST FOR STRANGE MODES TESTING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage 
 Data is from:
dataPawMalloc2: \\
	MallocParaKData0.dat \\
	MallocParaKData6.dat \\
	MallocParaKData7.dat \\
	MallocParaKData8.dat 
\newline
\begin{figure}[ht!]
\begin{center}

<<label=alloccomp,echo=FALSE,include=TRUE,cache=TRUE,dev='png',fig.width=6,fig.height=6>>=
p <- ggplot(data = dataf[STRIDE==min(STRIDE),], aes(SIZE, BANDWIDTH)) +  geom_point(colour = "red", size = 1,shape = 2, alpha=.1) 
if (cache_line>=10)p <- p +
  geom_vline(xintercept = L1cache, colour="yellow", linetype="dashed", size=0.7) + 
  geom_text(aes(x2,y2,label = texthere, hjust=-.1),colour="yellow",data.frame(x2=L1cache, y2=max(BANDWIDTH), texthere="L1 cache")) 
if ((cache_line %% 10)==2)p <- p +
  geom_vline(xintercept = L2cache, colour="yellow", linetype="dashed", size=0.7) + 
  geom_text(aes(x2,y2,label = texthere, hjust=-.1),colour="yellow",data.frame(x2=L2cache, y2=max(BANDWIDTH), texthere="L2 cache")) 
p <- p + 
     #line_stat(mean, geom="line", linetype="dotted") +
     #line_stat(max, geom="line", linetype="solid") + 
     scale_y_continuous("BANDWIDTH (MB/s)") +
     scale_x_continuous("MEMORY SIZE (KB)")

p <- p + facet_wrap(~CYCLES,ncol=2) 
 p
	garbage <- dev.off() 
	@	
\end{center}
\caption{\Sexpr{cap_name(min(STRIDE))}}
\end{figure}

\end{document}